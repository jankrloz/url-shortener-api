from flask import request
from flask_restful import Resource

from src.commands.urls.generate_shortened_url import generate_shortened_url
from src.commands.urls.resolve_shortened_url import resolve_shortened_url


class ShortenUrl(Resource):
    def post(self):
        data = request.get_json()
        return generate_shortened_url(data)


class ResolveUrl(Resource):
    def get(self, url):
        return resolve_shortened_url(url)
