from src.routes.blueprint import urls_api, urls_blueprint

blueprints = [urls_blueprint]

apis = [urls_api]
