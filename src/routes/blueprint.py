from flask import Blueprint
from flask_restful import Api

from src.routes.urls import ResolveUrl, ShortenUrl

urls_blueprint = Blueprint("urls", __name__, url_prefix="/urls")
urls_api = Api(urls_blueprint)

urls_api.add_resource(ResolveUrl, "/<string:url>")
urls_api.add_resource(ShortenUrl, "/")
