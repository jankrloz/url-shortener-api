import os

import boto3

URLS_TABLE = os.environ["URLS_TABLE"]
IS_OFFLINE = os.environ.get("IS_OFFLINE")

if IS_OFFLINE:
    client = boto3.client(
        "dynamodb", region_name="localhost", endpoint_url="http://localhost:8000"
    )
else:
    client = boto3.client("dynamodb")
