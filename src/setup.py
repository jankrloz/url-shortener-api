from datetime import datetime

from flask import Flask, jsonify

from src.routes import apis, blueprints


def create_application(name):
    app = Flask(name)

    app.url_map.strict_slashes = False

    for api in apis:
        add_json_representation(app, api)
    for blueprint in blueprints:
        app.register_blueprint(blueprint)

    return app


def add_json_representation(app, api):
    @api.representation("application/json")
    def output_json(data, code, headers=None):
        if code in (400, 401, 404, 500):
            response_format = {"error": data, "time": str(datetime.now())}
        else:
            response_format = {"data": data, "time": str(datetime.now())}

        resp = app.make_response(jsonify(**response_format))
        resp.status_code = code
        resp.headers.extend(headers or {})
        return resp

    return api
