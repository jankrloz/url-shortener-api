from src.services.dynamodb import URLS_TABLE
from src.services.dynamodb import client as dynamodb


def generate_shortened_url(data):

    url = data.get("url")
    if not url:
        return "Please provide url", 400

    dynamodb.put_item(
        TableName=URLS_TABLE, Item={"urlHash": {"S": "ASJHgd"}, "url": {"S": url}}
    )

    return {"url": url}
