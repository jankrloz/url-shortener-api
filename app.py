from src.setup import create_application

app = create_application(__name__)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
