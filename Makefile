SHELL=/bin/bash

ifndef VERBOSE
.SILENT:
endif


install:
	if [ ! -d venv ]; then python3 -m venv --copies venv; fi;
	source ./venv/bin/activate
	pip install -r requirements.txt
	npm install


start:	
	npx sls dynamodb start &
	npx sls wsgi serve &



black:
	black . --exclude="/(venv|node_modules)/" --check

isort:
	isort --recursive --skip venv --skip node_modules --check-only

autoflake:
	autoflake --recursive --exclude venv,node_modules --check --remove-all-unused-imports --remove-unused-variables ./ \

mypy:
	mypy app.py src

lint-fix:
	autoflake --recursive --exclude venv,node_modules --in-place --remove-all-unused-imports ./
	black . --exclude="/(venv|node_modules)/"
	isort --recursive --apply --skip venv --skip node_modules

lint: autoflake black isort
